<?php

namespace Drupal\published_node_revisions\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An example controller.
 */
class PublishedNodeRevisions extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer, EntityRepositoryInterface $entity_repository) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('entity.repository')
    );
  }

  /**
   * Generates an overview table of older revisions of a node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node object.
   *
   * @return array
   *   An array as expected by \Drupal\Core\Render\RendererInterface::render().
   */
  public function viewPublishedNodeRevisions(NodeInterface $node) {

    $langcode = $node->language()->getId();
    $langname = $node->language()->getName();
    $languages = $node->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $node_storage = $this->entityTypeManager()->getStorage('node');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $node->label(),
    ]) : $this->t('Revisions for %title', ['%title' => $node->label()]);

    $header = [$this->t('Revision ID'), $this->t('Published on')];

    $rows = [];

    $revisionIds = $this->entityTypeManager()->getStorage('node')->revisionIds($node);
    $revisionIds = array_reverse($revisionIds);

    foreach ($revisionIds as $vid) {

      /** @var \Drupal\node\NodeInterface $revision */
      $revision = $node_storage->loadRevision($vid);

      if ($revision->isPublished()) {

        // Only show revisions that are affected by the language of the node
        // we are looking at.
        $hasTranslation = $revision->hasTranslation($langcode);
        $revisionTranslationAffected = $revision->getTranslation($langcode)->isRevisionTranslationAffected();

        if ($hasTranslation && $revisionTranslationAffected) {

          $username = [
            '#theme' => 'username',
            '#account' => $revision->getRevisionUser(),
          ];

          // Use revision link to link to revisions that are not active.
          $date = $this->dateFormatter->format($revision->revision_timestamp->value, 'short');

          $link = Link::fromTextAndUrl($vid, new Url('entity.node.revision', [
            'node' => $node->id(),
            'node_revision' => $vid,
          ]))->toString();

          $row = [];
          $column = [
            'data' => [
              '#type' => 'inline_template',
              '#template' => '{{ revisionId }}',
              '#context' => [
                'revisionId' => $link,
              ],
            ],
          ];
          $row[] = $column;

          $link = Link::fromTextAndUrl($date, new Url('entity.node.revision', [
            'node' => $node->id(),
            'node_revision' => $vid,
          ]))->toString();

          $column = [
            'data' => [
              '#type' => 'inline_template',
              '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
              '#context' => [
                'date' => $link,
                'username' => $this->renderer->renderPlain($username),
                'message' => [
                  '#markup' => $revision->revision_log->value,
                  '#allowed_tags' => Xss::getHtmlTagList(),
                ],
              ],
            ],
          ];

          // @todo Simplify once https://www.drupal.org/node/2334319 lands.
          $this->renderer->addCacheableDependency($column['data'], $username);
          $row[] = $column;

          $rows[] = $row;
        }
      }
    }

    $build['node_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#attached' => [
        'library' => ['node/drupal.node.admin'],
      ],
      '#attributes' => ['class' => 'node-revision-table'],
    ];

    $build['pager'] = ['#type' => 'pager'];

    return $build;
  }

}
